public class Mahasiswa {		// mendefinisikan kelas Mahasiswa
    private String nim;			//baris ke-2 sampai ke-6, kita mendefinisikan atribut dari kelas Mahasiswa 
    private String nama;		//yaitu nim, nama, semester, usia, dan krs.
    private int semester;
    private int usia;
    private String[] krs;

    public Mahasiswa(String nim, String nama, int semester, int usia, String[] krs) {
        this.nim = nim;			//Pada baris ke-8 sampai ke-13,mendefinisikan konstruktor untuk kelas 
        this.nama = nama;		//Mahasiswa yang akan digunakan untuk menginisialisasi nilai dari atribut.
        this.semester = semester;
        this.usia = usia;
        this.krs = krs;
    }

    public String getNim() {	//Pada baris ke-16 sampai ke-53, kita mendefinisikan metode getter dan setter untuk atribut 
        return nim;				//nim, nama, semester, usia, dan krs.
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public int getUsia() {
        return usia;
    }

    public void setUsia(int usia) {
        this.usia = usia;
    }

    public String[] getKrs() {
        return krs;
    }

    public void setKrs(String[] krs) {
        this.krs = krs;
    }

    public float hitungRataNilai(int[] nilai) {		//ada baris ke-56 sampai ke-62, kita mendefinisikan metode hitungRataNilai
        int total = 0;								//untuk menghitung rata-rata dari nilai yang ada pada array nilai.
        for (int i = 0; i < nilai.length; i++) {
            total += nilai[i];
        }
        return (float) total / nilai.length;
    }

    public void infoMahasiswa() {					//Pada baris ke-64 sampai ke-69, kita mendefinisikan metode infoMahasiswa untuk
        System.out.println("NIM: " + nim);			//menampilkan informasi dasar dari seorang mahasiswa yaitu nim, nama, semester, dan usia.
        System.out.println("Nama: " + nama);
        System.out.println("Semester: " + semester);
        System.out.println("Usia: " + usia);
    }

    public void infoKrsMahasiswa() {				//Pada baris ke-71 sampai ke-77, kita mendefinisikan metode infoKrsMahasiswa untuk menampilkan
        System.out.println("KRS: ");				//seluruh mata kuliah yang diambil oleh seorang mahasiswa.
        for (int i = 0; i < krs.length; i++) {
            System.out.println("- " + krs[i]);
        }
    }
}