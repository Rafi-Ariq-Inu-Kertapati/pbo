public class MahasiswaTransfer extends MahasiswaBaru {	//Mendefinisikan kelas MahasiswaTransfer yang merupakan subkelas dari kelas MahasiswaBaru.

    private String asalUniversitas;	//Mendefinisikan atribut asalUniversitas dengan tipe data String.

    public MahasiswaTransfer(String nim, String nama, int semester, int usia, String[] krs, String asalSekolah, String asalUniversitas) {
        super(nim, nama, semester, usia, krs, asalSekolah);
        this.asalUniversitas = asalUniversitas;
    }	//Baris 5-8: Membuat konstruktor untuk menginisialisasi objek MahasiswaTransfer dengan memanggil konstruktor superkelas MahasiswaBaru dan mengisi atribut asalUniversitas.

    public boolean ikutOspek() {
        System.out.println("Saya tidak ikut OSPEK karena saya sudah kuliah sebelumnya di universitas lain.");
        return false;
    }	//Baris 10-13: Mendefinisikan metode ikutOspek() yang mengembalikan false dan mencetak pesan "Saya tidak ikut OSPEK karena saya sudah kuliah sebelumnya di universitas lain.".

    public void infoMahasiswa() {
        super.infoMahasiswa();
        System.out.println("Asal Universitas: " + asalUniversitas);
        System.out.println("Status: Transfer");
    }	//Baris 15-19: Mendefinisikan metode infoMahasiswa() yang mencetak atribut dari superkelas MahasiswaBaru dan atribut asalUniversitas serta mencetak pesan "Status: Transfer".
}