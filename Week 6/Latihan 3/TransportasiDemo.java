import Transportasi.Mobil;	
import Transportasi.Bicycle;	

public class TransportasiDemo {	
    public static void main(String[] args) {	
        Mobil mobil = new Mobil();	
        mobil.info();	

        Bicycle sepeda = new Bicycle();	
        sepeda.info();	
    }
}
