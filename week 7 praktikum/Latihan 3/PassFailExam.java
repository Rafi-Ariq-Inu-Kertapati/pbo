public class PassFailExam extends PassFailActivity {
    private int numberOfQuestions;
    private double pointsPerQuestion;
    private int numberMissed;
    
    public PassFailExam(int questions, int missed, double minPassing) {
        super(minPassing);
        numberOfQuestions = questions;
        numberMissed = missed;
        pointsPerQuestion = 100.0 / numberOfQuestions;
        double numericScore = 100.0 - (missed * pointsPerQuestion);
        setScore(numericScore);
    }
    
    public double getPointsEach() {
        return pointsPerQuestion;
    }
    
    public int getNumMissed() {
        return numberMissed;
    }
}
