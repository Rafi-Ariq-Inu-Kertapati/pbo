public class PassFailActivity extends GradeActivity {
    private double minimumPassingScore;
    
    public PassFailActivity(double min) {
        minimumPassingScore = min;
    }
    
    public char getGrade() {
        char letterGrade;
        if (getScore() >= minimumPassingScore)
            letterGrade = 'P';
        else
            letterGrade = 'F';
        return letterGrade;
    }
}
