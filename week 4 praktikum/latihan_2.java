import java.util.Scanner;

public class RumusABC {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputLagi = "Y";
        while (inputLagi.equalsIgnoreCase("Y")) {
            System.out.print("Masukkan nilai a: ");
            double a = scanner.nextDouble();
            System.out.print("Masukkan nilai b: ");
            double b = scanner.nextDouble();
            System.out.print("Masukkan nilai c: ");
            double c = scanner.nextDouble();

            double D = Math.pow(b, 2) - 4 * a * c;
            if (D > 0) {
                double X1 = (-b + Math.sqrt(D)) / (2 * a);
                double X2 = (-b - Math.sqrt(D)) / (2 * a);
                System.out.println("Akar-akar persamaan kuadrat adalah:");
                System.out.println("X1 = " + X1);
                System.out.println("X2 = " + X2);
            } else if (D == 0) {
                double X = -b / (2 * a);
                System.out.println("Akar-akar persamaan kuadrat adalah:");
                System.out.println("X1 = X2 = " + X);
            } else {
                double realPart = -b / (2 * a);
                double imaginaryPart = Math.sqrt(-D) / (2 * a);
                System.out.println("Akar-akar persamaan kuadrat adalah:");
                System.out.println("X1 = " + realPart + " + " + imaginaryPart + "i");
                System.out.println("X2 = " + realPart + " - " + imaginaryPart + "i");
            }

            System.out.print("Input data lagi [Y/T]? ");
            inputLagi = scanner.next();
        }
    }
}
