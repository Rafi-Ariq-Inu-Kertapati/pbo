import java.util.Scanner;

public class KonversiDetik {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char ulang;
        
        do {
            System.out.print("Detik\t: ");
            int detik = input.nextInt();
            
            KonversiWaktu kw = new KonversiWaktu(detik);
            
            System.out.println("Hari\t: " + kw.getHari());
            System.out.println("Jam\t: " + kw.getJam());
            System.out.println("Menit\t: " + kw.getMenit());
            System.out.println("Detik\t: " + kw.getSisaDetik());
            
            System.out.print("Input data lagi [Y/T]? ");
            ulang = input.next().charAt(0);
        } while (ulang == 'Y' || ulang == 'y');
    }
    
    static class KonversiWaktu {
        private int detik;
        
        public KonversiWaktu(int detik) {
            this.detik = detik;
        }
        
        public int getHari() {
            return detik / 86400;
        }
        
        public int getJam() {
            return (detik % 86400) / 3600;
        }
        
        public int getMenit() {
            return ((detik % 86400) % 3600) / 60;
        }
        
        public int getSisaDetik() {
            return ((detik % 86400) % 3600) % 60;
        }
    }
}
