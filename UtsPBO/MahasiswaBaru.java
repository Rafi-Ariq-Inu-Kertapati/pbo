public class MahasiswaBaru extends Mahasiswa {	//Mendefinisikan kelas MahasiswaBaru sebagai turunan dari kelas Mahasiswa.
    private String asalSekolah;	//Mendefinisikan variabel asalSekolah dengan tipe data String dan modifier private.

    public MahasiswaBaru(String nim, String nama, int semester, int usia, String[] krs, String asalSekolah) {	//Mendefinisikan konstruktor kelas MahasiswaBaru dengan parameter nim, nama, semester, usia, krs, dan asalSekolah.
        super(nim, nama, semester, usia, krs);	//Memanggil konstruktor kelas induk Mahasiswa dengan parameter nim, nama, semester, usia, dan krs.
        this.asalSekolah = asalSekolah;	//Memberikan nilai pada variabel asalSekolah dengan nilai yang diberikan pada parameter asalSekolah.
    }

    public String getAsalSekolah() {	//Mendefinisikan method getter untuk variabel asalSekolah.
        return asalSekolah;
    }

    public void setAsalSekolah(String asalSekolah) {	//Mendefinisikan method setter untuk variabel asalSekolah.
        this.asalSekolah = asalSekolah;
    }

    public boolean ikutOspek() {	//Mendefinisikan method ikutOspek() dengan tipe data boolean.
        // implementasi untuk ikut ospek
		return true;
    }

    @Override	//Anotasi yang menandakan bahwa method yang mengikuti anotasi ini akan menimpa (override) method dengan nama yang sama pada kelas induk.
    public void infoMahasiswa() {	//Mendefinisikan method infoMahasiswa() dengan tipe data void.
        super.infoMahasiswa();		//Memanggil method infoMahasiswa() pada kelas induk Mahasiswa.
        System.out.println("Asal Sekolah: " + asalSekolah);	//Menampilkan informasi tentang variabel asalSekolah.
    }
}