class Karyawan {
  private String nama;
  private int jumlahAnak;
  private double tunjanganAnak;

  public Karyawan(String nama, int jumlahAnak, double tunjanganAnak) {
    this.nama = nama;
    this.jumlahAnak = jumlahAnak;
    this.tunjanganAnak = tunjanganAnak;
  }

  public double getTunjanganAnak() {
    return tunjanganAnak;
  }

  public void setTunjanganAnak(double tunjanganAnak) {
    this.tunjanganAnak = tunjanganAnak;
  }

  public String getNama() {
    return nama;
  }

  public int getJumlahAnak() {
    return jumlahAnak;
  }

  public void setJumlahAnak(int jumlahAnak) {
    this.jumlahAnak = jumlahAnak;
  }
}

class KaryawanTetap extends Karyawan {
  private double gajiPokok;

  public KaryawanTetap(String nama, int jumlahAnak, double tunjanganAnak, double gajiPokok) {
    super(nama, jumlahAnak, tunjanganAnak);
    this.gajiPokok = gajiPokok;
  }

  public double hitungTotalGaji() {
    return gajiPokok + (getTunjanganAnak() * getJumlahAnak());
  }
}

class KaryawanKontrak extends Karyawan {
  private double upahHarian;
  private int jumlahHari;

  public KaryawanKontrak(String nama, int jumlahAnak, double tunjanganAnak, double upahHarian, int jumlahHari) {
    super(nama, jumlahAnak, tunjanganAnak);
    this.upahHarian = upahHarian;
    this.jumlahHari = jumlahHari;
  }

  public double hitungTotalUpah() {
    return (upahHarian * jumlahHari) + (getTunjanganAnak() * getJumlahAnak());
  }
}

class Main {
  public static void main(String[] args) {
    KaryawanTetap karyawanTetap = new KaryawanTetap("Budi", 2, 1000000, 5000000);
    System.out.println("Total gaji karyawan tetap: " + karyawanTetap.hitungTotalGaji());

    KaryawanKontrak karyawanKontrak = new KaryawanKontrak("Rina", 1, 500000, 200000, 20);
    System.out.println("Total upah karyawan kontrak: " + karyawanKontrak.hitungTotalUpah());
  }
}
