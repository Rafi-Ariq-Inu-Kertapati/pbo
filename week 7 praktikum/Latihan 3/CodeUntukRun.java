public class Main {
    public static void main(String[] args) {
        FinalExam finalExam = new FinalExam(50, 2);
        PassFailExam passFailExam = new PassFailExam(30, 5, 70.0);
        System.out.println("Final Exam - Score: " + finalExam.getScore() +
                           ", Grade: " + finalExam.getGrade() +
                           ", Points Per Question: " + finalExam.getPointsPerQuestion() +
                           ", Number Missed: " + finalExam.getNumberMissed());
        System.out.println("Pass/Fail Exam - Score: " + passFailExam.getScore()
