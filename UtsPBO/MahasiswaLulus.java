public class MahasiswaLulus extends Mahasiswa {	//Mendefinisikan class MahasiswaLulus yang merupakan subclass dari class Mahasiswa.
    private int tahunWisuda;	//Baris 2-3: Mendefinisikan dua atribut private yaitu tahunWisuda dan ipk.
    private float ipk;

    public MahasiswaLulus(String nim, String nama, int semester, int usia, String[] krs, int tahunWisuda, float ipk) {
        super(nim, nama, semester, usia, krs);	//Baris 5-10: Membuat constructor untuk class MahasiswaLulus yang menerima enam parameter yaitu nim, nama, semester, usia, krs, tahunWisuda, dan ipk.
        this.tahunWisuda = tahunWisuda;	//Constructor ini akan memanggil constructor superclass (Mahasiswa) dengan keyword super dan menginisialisasi nilai atribut tahunWisuda dan ipk.
        this.ipk = ipk;
    }

    public int getTahunWisuda() {	//Method getter untuk atribut tahunWisuda.
        return tahunWisuda;
    }

    public void setTahunWisuda(int tahunWisuda) {	//Method setter untuk atribut tahunWisuda.
        this.tahunWisuda = tahunWisuda;
    }

    public float getIpk() {	//Method getter untuk atribut ipk.
        return ipk;
    }

    public void setIpk(float ipk) {	//Method setter untuk atribut ipk.
        this.ipk = ipk;
    }

    public boolean ikutWisuda() {	//Method ikutWisuda() yang mengembalikan nilai boolean true. Method ini akan diimplementasikan dengan logika program yang sesuai.
        // implementasi untuk ikut wisuda
		return true;
    }

    @Override	//Anotasi yang menandakan bahwa method yang mengikuti anotasi ini akan menimpa (override) method dengan nama yang sama pada kelas induk.
    public void infoMahasiswa() {	//Mendefinisikan method infoMahasiswa() dengan tipe data void.
        super.infoMahasiswa();	////Memanggil method infoMahasiswa() pada kelas induk Mahasiswa.
        System.out.println("Tahun Wisuda: " + tahunWisuda); //Menampilkan informasi tentang variabel Tahun Wisuda.
        System.out.println("IPK: " + ipk);	////Menampilkan informasi tentang variabel IPK.
    }
}