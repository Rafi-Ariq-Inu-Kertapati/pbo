class Mobil2 {
  public void hidupkanMobil() {
    System.out.println("Mobil dihidupkan");
  }

  public void ubahGigi(int gigi) {
    System.out.println("Gigi diubah menjadi " + gigi);
  }

  public void matikanMobil() {
    System.out.println("Mobil dimatikan");
  }
}

class Mobil2BMW extends Mobil2 {
  public void nontonTV() {
    System.out.println("TV dihidupkan");
    System.out.println("TV mencari channel");
    System.out.println("TV menampilkan gambar");
  }
}

class Mobil2BMWDemo {
  public static void main(String[] args) {
    Mobil2BMW mobil = new Mobil2BMW();
    mobil.hidupkanMobil();
    mobil.nontonTV();
    mobil.ubahGigi(2);
    mobil.matikanMobil();
  }
}
