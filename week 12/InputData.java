import java.sql.*;

public class BarangController {
    private Connection connection;
    private Statement statement;

    public BarangController() {
        try {
            // Membuat koneksi ke database
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/nama_database", "username", "password");
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void inputData(String kode) {
        try {
            // Mengecek apakah kode barang sudah ada di database
            String query = "SELECT * FROM nama_tabel WHERE kode_barang = '" + kode + "'";
            ResultSet resultSet = statement.executeQuery(query);

            if (resultSet.next()) {
                // Jika kode barang sudah ada, tampilkan message dialog bahwa kode sudah ada
                System.out.println("Kode barang sudah ada dalam database.");
            } else {
                // Jika kode barang belum ada, lakukan proses input
                // Lakukan proses input data ke database sesuai kebutuhan Anda
            }

            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
