import java.sql.*;

public class BarangController {
    private Connection connection;
    private Statement statement;

    public BarangController() {
        try {
            // Membuat koneksi ke database
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/nama_database", "username", "password");
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteData(int id) {
        try {
            // Lakukan proses delete data di database berdasarkan kolom id
            String query = "DELETE FROM nama_tabel WHERE id = " + id;
            int rowsAffected = statement.executeUpdate(query);

            if (rowsAffected > 0) {
                System.out.println("Data berhasil dihapus.");
            } else {
                System.out.println("Data dengan id tersebut tidak ditemukan.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
