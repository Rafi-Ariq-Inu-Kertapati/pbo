import java.util.Scanner;

public class Penjualan {
    private String kode;
    private String nama;
    private float harga;
    private int jumlah;

    public void setData(String kode, String nama, float harga, int jumlah) {
        this.kode = kode;
        this.nama = nama;
        this.harga = harga;
        this.jumlah = jumlah;
    }

    public float getTotalPembelian() {
        return harga * jumlah;
    }

    public String getBonus() {
        float total = getTotalPembelian();
        if (total >= 500000 && jumlah > 5) {
            return "Setrika";
        } else if (total >= 100000 && jumlah > 3) {
            return "Payung";
        } else if (total >= 50000 || jumlah > 2) {
            return "Ballpoint";
        } else {
            return "Tidak ada bonus";
        }
    }

    public void cetakNota() {
        System.out.println("Kode Barang : " + kode);
        System.out.println("Nama Barang : " + nama);
        System.out.println("Harga Barang : " + harga);
        System.out.println("Jumlah Barang : " + jumlah);
        System.out.println("Total Pembelian : " + getTotalPembelian());
        System.out.println("Bonus : " + getBonus());
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputLagi = "Y";
        while (inputLagi.equalsIgnoreCase("Y")) {
            Penjualan penjualan = new Penjualan();
            System.out.print("Masukkan kode barang : ");
            String kode = scanner.nextLine();
            System.out.print("Masukkan nama barang : ");
            String nama = scanner.nextLine();
            System.out.print("Masukkan harga barang : ");
            float harga = scanner.nextFloat();
            System.out.print("Masukkan jumlah barang : ");
            int jumlah = scanner.nextInt();
            scanner.nextLine();
            penjualan.setData(kode, nama, harga, jumlah);
            penjualan.cetakNota();
            System.out.print("Input data lagi [Y/T] ? ");
            inputLagi = scanner.nextLine();
        }
    }
}
