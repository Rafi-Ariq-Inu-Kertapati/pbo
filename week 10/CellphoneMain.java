package implementasi.phone;

public class CellphoneMain {
    public static void main(String[] args) {
        Cellphone cp = new Cellphone("Samsung", "A33");
        
        cp.powerOn();
        cp.volumeUp();
        System.out.println("Volume: " + cp.getVolume());
        cp.volumeDown();
        System.out.println("Volume: " + cp.getVolume());
        
        cp.topUp(100000);
        System.out.println("Sisa Pulsa: " + cp.getBalance());
        
        cp.addContact("Rafi", "123456789");
        cp.addContact("Ariq", "987654321");
        
        cp.viewAllContacts();
        cp.searchContact("Rafi");
        cp.searchContact("Ariq");
        
        cp.powerOff();
    }
}
