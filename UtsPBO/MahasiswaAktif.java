public class MahasiswaAktif extends Mahasiswa {	//Membuat sebuah kelas Java baru dengan nama MahasiswaAktif yang merupakan subclass dari kelas Mahasiswa.

    public MahasiswaAktif(String nim, String nama, int semester, int usia, String[] krs) {
        super(nim, nama, semester, usia, krs);
    }	//Baris 3-5: Mendefinisikan konstruktor kelas MahasiswaAktif dengan menerima parameter nim, nama, semester, usia, dan krs. Konstruktor ini akan memanggil konstruktor superclass Mahasiswa menggunakan kata kunci "super".

    public void infoMahasiswa() {
        super.infoMahasiswa();
        System.out.println("Status: Aktif");
    }	//Baris 7-11: Mengimplementasikan metode "infoMahasiswa" yang merupakan metode turunan dari kelas Mahasiswa. Metode ini akan memanggil metode "infoMahasiswa" dari superclass Mahasiswa menggunakan kata kunci "super", kemudian menampilkan status "Aktif" menggunakan System.out.println().
}