package implementasi.phone;
import java.lang.Math;
import java.util.ArrayList;
import java.util.List;

public class Cellphone implements Phone {
    String merk;
    String type;
    int batteryLevel;
    int status;
    int volume;
    int balance;
    List<Contact> contacts;

    public Cellphone(String merk, String type) {
        this.merk = merk;
        this.type = type;
        this.batteryLevel = (int) (Math.random() * (100 - 0 + 1) + 0);
        this.contacts = new ArrayList<>();
    }

    public void powerOn() {
        this.status = 1;
        System.out.println("Ponsel menyala");
    }

    public void powerOff() {
        this.status = 0;
        System.out.println("Ponsel mati");
    }

    public void volumeUp() {
        if (this.status == 0) {
            System.out.println("Ponsel mati. Tidak dapat menaikkan volume");
        } else {
            this.volume++;
            if (this.volume >= MAX_VOLUME) {
                this.volume = MAX_VOLUME;
            }
        }
    }

    public void volumeDown() {
        this.volume--;
        if (this.volume <= MIN_VOLUME) {
            this.volume = MIN_VOLUME;
        }
    }

    public int getVolume() {
        return this.volume;
    }

    public void topUp(int amount) {
        this.balance += amount;
        System.out.println("Pulsa berhasil diisi sebesar " + amount);
    }

    public int getBalance() {
        return this.balance;
    }

    public void addContact(String name, String number) {
        Contact contact = new Contact(name, number);
        contacts.add(contact);
        System.out.println("Kontak berhasil ditambahkan: " + name + " - " + number);
    }

    public void viewAllContacts() {
        System.out.println("Daftar Kontak:");
        for (Contact contact : contacts) {
            System.out.println(contact.getNama() + " - " + contact.getNomor());
        }
    }

    public void searchContact(String name) {
        System.out.println("Hasil Pencarian Kontak:");
        boolean found = false;
        for (Contact contact : contacts) {
            if (contact.getNama().equalsIgnoreCase(name)) {
                System.out.println(contact.getNama() + " - " + contact.getNomor());
                found = true;
            }
        }
        if (!found) {
            System.out.println("Kontak dengan nama " + name + " tidak ditemukan.");
        }
    }
}
