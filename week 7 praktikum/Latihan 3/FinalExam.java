public class FinalExam extends GradeActivity {
    private int numberOfQuestions;
    private int numberMissed;
    private double pointsPerQuestion;
    
    public FinalExam(int questions, int missed) {
        numberOfQuestions = questions;
        numberMissed = missed;
        pointsPerQuestion = 100.0 / numberOfQuestions;
        double numericScore = 100.0 - (missed * pointsPerQuestion);
        setScore(numericScore);
    }
    
    public double getPointsPerQuestion() {
        return pointsPerQuestion;
    }
    
    public int getNumberMissed() {
        return numberMissed;
    }
}
